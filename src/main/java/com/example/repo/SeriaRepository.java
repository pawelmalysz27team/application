package com.example.repo;


import org.springframework.data.repository.CrudRepository;

import com.example.model.Seria;

public interface SeriaRepository extends CrudRepository<Seria, Long>{
	
}
