package com.example.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.model.Seria;
import com.example.repo.SeriaRepository;


@Controller
public class WebController {
	
	@Autowired
	SeriaRepository repo;
	
	@RequestMapping("/")
	public String showTable(Model model)
	{
		List<Seria> result = new ArrayList<Seria>();
		
		for(Seria s : repo.findAll())
		{
			result.add(s);
		}
		
		model.addAttribute("result",result);
		model.addAttribute("ddd","test");
		return "index";
	}
}
